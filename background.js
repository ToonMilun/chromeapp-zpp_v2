chrome.runtime.onInstalled.addListener(function() {

	/********************/
	/* GLOBAL VARIABLES */
	/********************/
	
	async function storeHandlebars()
	{
		let templatesDict = {};
		await loadTextData("src/templates/bugs-ui.hbs", templatesDict);

		chrome.storage.local.set({
			templates: templatesDict
		},
		function() {
			console.log("Complete: chrome.storage.local.set");
		});
	}

	function loadTextData(url, dict) {
		
		return new Promise(resolve => {
			$.ajax({
				url: url,
				crossDomain: false,
				cache: true,
				dataType: "text"})
			.done((data) => {

				// FIXME: Bad way of getting filename.
				let name = url.split("/");
				name = name[name.length-1].split(".");
				name = name[0];
				
				dict[name] = data;
			})
			.fail(() => {
				alert("Ajax error! Could not load \"" + url + "\".");
			})
			.always(() => {
				resolve();
			});
		});
	}

	storeHandlebars();











	//storeInterfaceHTML();

	// Store the HTML needed for the bugWindowForm in chrome.storage (to allow it to communicate with the content scripts).
	function storeInterfaceHTML()
	{
		// Needs jQuery to be included in the background.scripts array of manifest.json.
		$.ajax({url: 'src/ui/html/interface.html',
			crossDomain: false,
			cache: true,
			dataType: "html",
			success: function(data) {
				
				chrome.storage.local.set({
					interfaceHTMLData: data
				},
				function() {
					console.log("Complete: chrome.storage.local.set");
				});
			},
			error: function(e)
			{
				alert("Ajax error! Could not load \"interface.html\".");
			},
			complete: function(e)
			{
				
			}
		});
	}
	
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [new chrome.declarativeContent.PageStateMatcher({
          pageUrl: {hostEquals: 'https://*.projects.zoho.com/*'}, // Make extension only available when on this site.
        })
        ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
});