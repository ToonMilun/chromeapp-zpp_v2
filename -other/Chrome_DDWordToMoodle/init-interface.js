(function () {

	/**
	 * Called as soon as the play button is clicked from the popup.html.
	 */
	$(document).ready(function(){
		
		initWindow();
	});

	function initWindow()
	{
		var popupWindow = window.open("", "", "width=460,height=600");
		popupWindow.document.title = "[DD Word to Moodle]";
		popupWindow.document.close(); // Allow the window html to be edited now.
		
		var $window = $(popupWindow.document.body);
		
		// Load the Chrome apps stored HTML file into the popup (stored in background.js).
		chrome.storage.sync.get('interfaceHTMLData', function(data){

			console.log(data.interfaceHTMLData);
			$window.html(data.interfaceHTMLData);
		});
	}

})();