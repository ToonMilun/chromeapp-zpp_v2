chrome.runtime.onInstalled.addListener(function() {

	/********************/
	/* GLOBAL VARIABLES */
	/********************/
	
	storeInterfaceHTML();

	// Store the HTML needed for the bugWindowForm in chrome.storage (to allow it to communicate with the content scripts).
	function storeInterfaceHTML()
	{
		// Needs jQuery to be included in the background.scripts array of manifest.json.
		$.ajax({url: 'src/interface/html/interface.html',
			crossDomain: false,
			cache: true,
			dataType: "html",
			success: function(data) {
				
				chrome.storage.local.set({
					interfaceHTMLData: data
				},
				function() {
					console.log("Complete: chrome.storage.local.set");
				});
			},
			error: function(e)
			{
				alert("Ajax error! Could not load \"interface.html\".");
			},
			complete: function(e)
			{
				
			}
		});
	}
	
	chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [new chrome.declarativeContent.PageStateMatcher({
          pageUrl: {hostEquals: 'aws.didasko-online.com'}, // Make extension only available when on this site.
        })
        ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
});