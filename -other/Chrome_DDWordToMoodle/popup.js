let btnRunScripts = document.getElementById('runScripts');

/**
 * This is a multi-page script, and it needs to use an external popup window as the interface across pages.
 * To (re)start the process, clicking the play button from the extension is needed.
 * Clicking this button will set a Chrome value to notify the scripts (which are called on each pageload) that they are being reset.
 */
btnRunScripts.onclick = function(element) {
	
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		chrome.tabs.executeScript(tabs[0].id, {
			file: "src/extension/scripts/newTask.js"
		});
	});
};