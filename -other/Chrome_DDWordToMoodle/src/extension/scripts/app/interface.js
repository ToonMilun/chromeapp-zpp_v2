/**
 * Popup window with the main UI.
 * 
 * This class handles creating + getting the interface.
 * 
 * Since passing complex data between pages is a bit of a challenge, I've decided to store relevant information inside of the popup window, and retrieve it on each pageload
 * width window.open().
 * 
 * Variables can also be passed around this way, as they can be stored in the window object of the popup.
 * 
 * Important note:
 * If the main window navigates away from the aws.didasko-online.com domain, then the link between the window and the page CANNOT be re-established (which is kind of good for
 * security reasons).
 */

define(["utils"], function(u){

	const MESSAGE_ORIGIN = "https://aws.didasko-online.com/";

	return function(createNewWindow, onloadCallback){

		const WINDOW_NAME = "dd-word-to-moodle-interface";
		const WINDOW_TITLE = "[DD Word to Moodle]";

		const WINDOW_WIDTH = 900;

		var _me = this;
		this.w = undefined;

		function init()
		{
			if (createNewWindow) createWindow();
			else 			     getWindow();
		}

		function windowOnLoad(){

			if (onloadCallback) onloadCallback(_me);

			// Post a message to the Interface to let it know about the main page.
			_me.w.postMessage(chrome.extension.getURL("src/interface/scripts/app/"), MESSAGE_ORIGIN);
		}

		function getWindow()
		{
			var popupWindow = window.open("", "dd-word-to-moodle-interface", "", true);
			_me.w = popupWindow;
			popupWindow.document.close();

			// Determine if this is indeed the popupwindow that was made on a previous pageload, or if this just created a new tab by accident.
			if (popupWindow.document.title != WINDOW_TITLE)
			{
				u.debugLogDebug("interface.js: No interface window found.");
				popupWindow.close(); // Close the new tab automatically.
				return;
			}

			u.debugLogSuccess("interface.js: Interface window found.");
			windowOnLoad();
		}

		function createWindow()
		{
			var popupWindow = window.open("", "dd-word-to-moodle-interface", "width=" + WINDOW_WIDTH + ",height=600,top=0,left=0", true);
			_me.w = popupWindow;

			// Determine if there's a window with this name already exists. If it does, stop.
			if (popupWindow.document.title == WINDOW_TITLE)
			{
				u.debugLogError("interface.js: An interface window already exists! Assigning its reference to main.js instead.");
				windowOnLoad();
				return;
			}

			////////////////
			// NEW WINDOW //
			////////////////

			popupWindow.document.title = WINDOW_TITLE;
			// Append a stylesheet.
			popupWindow.document.write('<link href="' + chrome.extension.getURL("src/interface/css/interface.css") + '" type="text/css" rel="stylesheet">'); 
			// JS scripts.
			popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/global/lib/jquery-2.1.4.min.js") + '"></script>');
			popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/global/lib/require/require.js") + '"></script>');
			popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/interface/scripts/app.js") + '"></script>');
			// Allow the window html to be edited now.
			popupWindow.document.close();
			
			// Wait for the base HTML to be ready before continuing.
			// Must be done with plain Javascript (otherwise, the event is sometimes at the wrong time).
			// https://stackoverflow.com/questions/4842432/jquery-receive-document-ready-on-child-window
			popupWindow.onload = function(){
				var $window = $(popupWindow.document.body);
			
				// Load the Chrome apps stored HTML file into the popup (see: background.js).
				chrome.storage.local.get('interfaceHTMLData', function(data){
					$window.html(data.interfaceHTMLData); // Populate the body with the interface.html data.
					windowOnLoad();
					windowOnCreate();
				});
			};
		}

		function windowOnCreate(){

			
		}

		init();
	};
});







