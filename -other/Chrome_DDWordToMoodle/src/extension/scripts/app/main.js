/**
 * Main.
 */

define(["utils", "lib/jszip/jszip-utils.min", "lib/jszip/jszip.min", "lib/x2js/xml2json.min", "interface"], function(u, JSZipUtils, JSZip, X2JS, Interface){

	return function(){

		/**
		 * newTask = true		the user just clicked the play button from the extension popup.html. Create a new popup window ans start from the start.
		 * newTask = false		there is already a task in progress, and the page was just changed. Get data from Chrome to regain continuity.
		 */
		new Interface(false);
	};

	return;

	Interface();

	//$("body").css("background-color", "red");

	/*setTimeout(function(){
		$(".gb_d")[1].click();
	}, 1000);*/

	// Ok. New plan. Export each Word doc as a .html, then use jQuery to filter it.

	var docHTMLPath = chrome.extension.getURL("src/media/test.htm");

	$.ajax({
        
		type: "GET",
	  	url: docHTMLPath,
		cache: false
        
	}).done(function(htmlData) {

		var $holder = $("<iframe src='" + docHTMLPath + "' style='position: fixed;'></iframe>");
		//$holder.html(htmlData);
		$("body").append($holder);

	}).fail(function(){
		console.log("UNSUCCESSFUL");
	});


	return;

	/**
	 * .docx documents are .zips. So to get the text from one, it must first be extracted.
	 */
	var docxPath = chrome.extension.getURL("src/media/test.docx");
	zip = new JSZip();

	//var fs = require("fs");

	function searchForTag(tagCur, tagName)
	{
		if (typeof tagCur !== "object") return;

		$.each(tagCur, function(i, e){
			if (i == "tc") console.log(e);
			searchForTag(e, tagName);
		});
	}

	// Get the raw zip data from the .docx
	JSZipUtils.getBinaryContent(docxPath, function(err, data) {
		if(err) {
			throw err;
		}
	
		JSZip.loadAsync(data).then(function (z) {
			
			// The Word document information that we're interested in is contained in the following XML.
			var document = z.files["word/document.xml"];

			return document.async("text");
		}).then(function(txt){
			//console.log("the content is", txt);

			var x2js = new X2JS();

			var jsonObj = x2js.xml_str2json(txt);
			
			$.each(jsonObj.document.body.tbl, function(i, e){
				console.log("XXXXXXX");
				console.log("TABLE: " + i);
				console.log("XXXXXXX");

				searchForTag(e, "__text");
			});

			console.log("DONE");
		});
	});	



	/*zip.workerScriptsPath = chrome.extension.getURL("src/scripts/lib/zipjs");

	

	$.ajax({
        
		type: "GET",
	  	url: docxLocation,
		cache: false
        
	}).done(function(docxData) {

		console.log("DID IT READ ANYTHING?");
		console.log(docxData);
	}).fail(function(){
		console.log("UNSUCCESSFUL");
	});

	console.log(zip);
	console.log();
	console.log(zip.Data64URIReader(chrome.extension.getURL("src/media/test.docx")));
	*/


	function init()
	{
		//var interfaceWindow = initWindow();

		// Store globally (other scripts will need to access this a lot).
		//$.interfaceWindow = interfaceWindow;
	}

	function initWindow()
	{
		var popupWindow = window.open("", "", "width=460,height=600,top=0,left=0");
		popupWindow.document.title = "[DD Word to Moodle]";
		popupWindow.document.close(); // Allow the window html to be edited now.
		
		var $window = $(popupWindow.document.body);
		
		// Load the Chrome apps stored HTML file into the popup (see: background.js).
		chrome.storage.sync.get('interfaceHTMLData', function(data){
			$window.html(data.interfaceHTMLData);
		});

		return popupWindow;
	}

	init();
});







