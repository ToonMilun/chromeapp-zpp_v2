/**
 * (Re)start the task.
 */

chrome.storage.local.set({newTask: true}, function(){
  require(["Injector!newTask"], function(newTask){
    new newTask();
  });
});