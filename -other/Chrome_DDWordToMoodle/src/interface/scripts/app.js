/**
 * Scripts for the Interface Window.
 */

(function(){

  const MESSAGE_ORIGIN = "https://aws.didasko-online.com";
  var requireInitialized = false;;

  function setParentWindow(w)
  {
    $w = function(selector){
      return $(selector, w.document);
    };

    if ($.debugLog) $.debugLogPageChange("app", "Parent window reference updated.");

    // Trigger a jQuery event on the window each time the page changes.
    $(window).trigger("parentWindowChanged");
  }

  $.oneParentWindowChanged = function(callback){
    if ($.debugLogDebug) $.debugLogDebug("", "Waiting for the page to change before proceeding.");
    $(window).one("parentWindowChanged", callback);
  }

  function throwSecurityError(){
    alert("CRITICAL SECURITY ERROR!!!\nCRITICAL SECURITY ERROR!!!\nCRITICAL SECURITY ERROR!!!\n\nMessage received from postMessage does NOT match accepted formatting! It's possible there is a malicious script intefering with this extension! Call someone from the I.T. crew IMMEDIATELY!\n\nDo NOT attempt to continue using this extension until this issue is resolved.");
    throw "Critical security error!";
  }

  // Whenever the main page changes, it will send a message to the Interface to update its page reference.
  function receiveMessage(event)
  {
    // Security check.
    if (event.origin !== MESSAGE_ORIGIN) throwSecurityError();
    try {
      if (event.data.indexOf("chrome-extension://") !== 0) throwSecurityError();
    }
    catch (err) {
      throwSecurityError();
    }

    // Record a reference to the current page.
    setParentWindow(event.source);

    // Start requirejs (only once).
    if (!requireInitialized)
    {
      requireInitialized = true;
      initInterfaceWindow(event.data);
    }
  }

  // Wait for the extension to send a message containing the baseUrl.
  window.addEventListener("message", receiveMessage, false);

  // Function called by extension when it first creates the Interface window.
  function initInterfaceWindow(baseUrl){

    console.log("%c Loading scripts from: " + baseUrl, "color: orange;");

    requirejs.config({
        baseUrl: baseUrl, // Value set by extension/scripts/app/interface.js.
        paths: {
            lib: "../../../global/lib",
            macro: "macros/macro"
        },
        /**
         * GLOBALS
         */
        config: {
            'utils': {
                GLOBAL: {

                }
            }
        }
    });

    /*******************************************************************************************
     * Author: Jeroen de Lau & Mr Lister
     * Source: https://stackoverflow.com/questions/9057292/requirejs-in-a-chrome-extension
     * Date accessed: 4th July 2019
     *******************************************************************************************/
    /*
    * Inject the plugin straight into requirejs
    */
    define("Injector", {
        load: function (name, req, onload, config) {
      
          //Load the script using XHR, from background
          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", function () {
      
            //Find depenencies in the script, and prepend the
            //Injector! plugin, forcing the load to go through this
            //plugin.
            var modified = getDeps(oReq.response)
      
            //have requirejs load the module from text
            //it will evaluate the define, and process dependencies
            onload.fromText(modified);
          });
          oReq.open("GET", req.toUrl(name) + ".js");
          oReq.send();
      
          //Find dependencies and prepend Injector!
          function getDeps(script)
          {
            //extract the define call, reduced to a single line
            var defineCall = script.match(/define([\s\S])*?{/m)[0].split("\n").join("");
            //extract dependenceis from the call
            var depsMatch = defineCall.match(/\[([\s\S]*?)\]/);
      
            //if there are dependencies, inject the injectors
            if (depsMatch)
            {
              var deps = depsMatch[0];
              var replaced = deps.replace(/(\'|\")([\s\S]*?)\1/g, '$1Injector!$2$1');
              return script.replace(/define([\s\S]*?)\[[\s\S]*?\]/m, 'define$1' + replaced);
            }
            //no dependencies, return script
            return script;
          }
        }
      });
      
    /**
     * Call all your dependencies using the plugin
     */
    requirejs(["Injector!main"], function(Main){
      new Main();
    });
  }
})();