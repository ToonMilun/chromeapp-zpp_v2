/**
 * Gets the settings from the Interface and runs all applicable macros on the main page.
 */

define(["file-scan"], function(FileScan){

	return function(){

		function init()
		{
			var $iframe = $("#file-holder");

			// Monitor the .htm file upload field.
			$("#setting-file").change(function(){
				//console.log($(this)[0].files);
				$iframe.attr("src", window.URL.createObjectURL($(this)[0].files[0]));
			});

			var $fileScanBtn = $("#file-scan-start");
			$fileScanBtn.click(function(){
				new FileScan($iframe.first());
			});
		}

		init();
	};
});