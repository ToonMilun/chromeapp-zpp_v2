/**
 * Interface Window main.
 * Called once, when the window is loaded.
 */

define(["popup", "main-settings", "main-macro"], function(Popup, MainSettings, MainMacro){

	const VERSION = "1.0.0"
	const MUTATIONOBSERVER_TIMEOUT = 10; // In seconds.

	return function(){

		function init(){
			extendJQuery();
			initWindowFunctions();

			$.debugLogPageChange("main", "Interface window created.");

			new Popup("DD Word to Moodle - Introduction", " \
				<p>\
					<strong>Author</strong>: Milton Plotkin<br />\
					<strong>Version</strong>: " + VERSION + "\
				</p>\
				<p>This extension is used to automate importing data from a specially formatted Assesment Word document into Moodle. If this is not what you intend to do, please close this extension.</p>\
				<p>Before proceeding, please read the following:</p>\
				<ul>\
					<li>Ensure you are using Chrome.</li>\
					<li>Make sure you are in the root page of the unit you wish to automate before clicking 'OK' below (note: it does not matter if \"editing\" is on or off. It will automatically be toggled if necessary).</li>\
					<li>If you close this popup window at any time, then any process it is in the middle of will be cancelled. <strong>Please only close the window when the indicator at the top says it is safe to do so.</strong></li>\
					<li><strong>While this extension is running, you <u>do not</u> interact with your browser in <u>any</u> way!</strong> This extension will be simulating keyboard inputs and mouse clicks, and any real input from a user could cause it to lose its place and be unable to continue its automation. Safeguards will be added in future, but until that point <em>please</em> do not interrupt this extension while it is running (it will tell you when it's safe to interact with the page again).</li>\
					<li>The are on the right is the debug area. This extension will report on most actions it performs in there so you have an idea of what it's up to. If something goes wrong, please take a snapshot of the latest message in the debug area and send it to a programmer to assist them with bugfixing.</li>\
					<li>Please keep in mind that in the early development of this extension, there is a chance that it may incorrectly enter input. It is adviseable that each automated change is checked for its validity. For your convenience, any value this extension changes will be marked.</li>\
				</ul>\
			", function(){
				
			});

			new MainSettings();

			$(".main-start").one("click", function(){
				new MainMacro();
			});
		}

		function extendJQuery()
		{
			$.fn.exists = function(){
				return $(this).length > 0;
			};

			// HTML mutation observer.
			$.fn.oneObserver = function(callback){

				$.debugLogDebug("", "Waiting for a HTML mutation before proceeding (timeout: " + MUTATIONOBSERVER_TIMEOUT + " seconds).");			
				var observerParent = $(this)[0];
			
				var mo;

				// Timeout if no observation occurs in MUTATIONOBSERVER_TIMEOUT seconds.
				var timeout = setTimeout(function(){
					$.debugLogWarning("", "MutationObserver timed out! Attempting to continue script regardless.");
					mo.disconnect();
				}, MUTATIONOBSERVER_TIMEOUT*1000);

				mo = new MutationObserver(function (mutations) {
					clearTimeout(timeout); // Cancel the timeout. The HTML mutation was observed.

					setTimeout(function(){
						$.debugLogDebug("", "HTML mutation observed.");
						callback();
					}, 100);
					this.disconnect();
				}).observe(observerParent, {
					attributes: true,
					childList: true,
					characterData: true
				});
			};

			/**
			 * Attempts to click the $(this)[0] element (and posts a message), or throws an error if there is no $(this).
			 */
			$.fn.macroClick = function(scriptName, elementName, suffix){
				
				if (!$(this).exists()) $.debugLogError(scriptName, "Could not find \"" + elementName + "\" button to click.");

				$(this)[0].click();
				$.debugLog(scriptName, "Clicked on \"" + elementName + "\"" + (suffix ? (" " + suffix) : ""));
			};

			/**
			 * Attempts to set the val() of the element. If successful, will $.fn.mark() it. If it is unsuccessful, will show a warning (but will allow the script to continue).
			 */
			$.fn.macroSetVal = function(val){
				if (!$(this).exists())
				{
					$.debugLogWarning("", "Could not find input\"#" + $(this).attr("id") + "\" input to set val().");
					return;
				}

				$(this).val(val).mark();
			};

			$.fn.macroSetChecked = function(bool){
				if (!$(this).exists())
				{
					$.debugLogWarning("", "Could not find checkbox input\"#" + $(this).attr("id") + "\" input to check/uncheck.");
					return;
				}

				$(this).each(function(){
					$(this).mark();

					if ($(this).is(":checked"))
					{
						if (!bool) $(this)[0].click();
					}
					else
					{
						if (bool) $(this)[0].click();
					}
				});
			};

			/**
			 * Marks an element which has been modified.
			 */
			$.fn.mark = function(){
				$(this).css({
					"-webkit-box-shadow": "0px 0px 0px 7px #DD00DD",
					"box-shadow": "0px 0px 0px 7px #DD00DD"
				});
			}
		}

		function testPageContinuity()
		{
			$.debugLog("main", "I will paint the parent page in 2 seconds.");
			setTimeout(function(){
				$w("body").css("background-color", "green");
				$.debugLog("main", "I will click on the \"Usage Report\" menu button in 2 seconds.");
				setTimeout(function(){
					$w("[title='Usage Report']")[0].click();

					$.oneParentWindowChanged(function(){
						$.debugLog("main", "I will paint the \"Usage Report\" page in 2 seconds.");
						setTimeout(function(){
							$w("body").css("background-color", "yellow");
							$.debugLog("main", "I will click back to \"Home\" page in 2 seconds.");
							setTimeout(function(){
								$w("[title='Home']")[0].click();
							}, 2000);
						}, 2000);
					});

				}, 2000);
			}, 2000);
		}
		
		function initWindowFunctions()
		{
			var $debugWindow = $(".debug-wrapper");

			// Allow access to Extensions.
			$.debugLog = function(scriptName, text, style){
				
				var d = new Date();

				var $div = $("<div>");
				$div.addClass("debug-item");

				var $script = $("<div>");
				$script
				.text(scriptName ? (scriptName + ".js") : "")
				.addClass("debug-item-script-name");

				var h = "" + d.getHours(); 		if (h.length != 2) h = "0" + h;
				var m = "" + d.getMinutes(); 	if (m.length != 2) m = "0" + m;
				var s = "" + d.getSeconds(); 	if (s.length != 2) s = "0" + s;

				var $time = $("<div>");
				$time
				.text("[" + h + ":" + m + ":" + s +"]")
				.addClass("debug-item-time");

				var $text = $("<div>");
				$text
				.text(text)
				.addClass("debug-item-text");

				if (style) $div.attr("style", style);

				$div.append($script).append($time).append($text);

				$debugWindow.append($div);

				// Scroll the debug window to the bottom to ensure the latest messages are visible.
				$debugWindow.scrollTop($debugWindow[0].scrollHeight);
			};

			$.debugLogWarning = function(scriptName, text){
				$.debugLog(scriptName, text + " | Attempting to stop script.", "color: orangered; font-weight: bold;");
			}

			// Also throws an error.
			$.debugLogError = function(scriptName, text){
				$.debugLog(scriptName, text + " | Attempting to stop script.", "color: red; font-weight: bold;");
				throw scriptName + ": " + text;
			}

			$.debugLogPageChange = function(scriptName, text){
				$.debugLog(scriptName, text, "color: #CC71CC; font-style: italic;");
			}

			$.debugLogDebug = function(scriptName, text){
				$.debugLog(scriptName, text, "color: #880088; font-style: italic;");
			}
			
			$.debugLogHeader = function(scriptName, text){
				$.debugLog(scriptName, text, "color: darkgreen; font-weight: bold;");
			}

			$.debugLogSuccess = function(scriptName, text){
				$.debugLog(scriptName, text, "color: green;");
			}
		}

		////////////
		init();
	};
});







