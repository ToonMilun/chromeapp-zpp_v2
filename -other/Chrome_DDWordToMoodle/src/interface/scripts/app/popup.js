/**
 * Creates a popup message that appears over the main-wrapper area. Can be closed by clicking the button.
 */

define([], function(){

	return function(title, body, onClickOK){

		function init()
		{
			var $p = $("<div class='main-popup'>");

			var $logo = $("<img class='main-popup-logo' src='" + require.toUrl('') +"../../assets/images/logo.png' />");
			$p.append($logo);

			$p.append("<h2>" + title + "</h2>");
			
			var $body = $("<div>");
			$body.html(body);
			$p.append($body);

			var $btn = $("<button>OK</button>");

			$p.append($btn);

			$(".main-wrapper").append($p);

			$btn.one("click", function(){
				$p.remove();

				if (onClickOK) onClickOK();
			});
		}

		init();
	};
});