/**
 * Gets the settings from the Interface and runs all applicable macros on the main page.
 */

define([
		"macros/editing-on",
		"macros/add-quiz",
		"macros/fill-quiz",
	], function(
		MEditingOn,
		MAddQuiz,
		MFillQuiz
	){

	return function(){

		var settings = {
			quiz: $("[name='setting-quiz']").is(":checked"),
			questions: $("[name='setting-questions']").is(":checked")
		};

		var m; // Always set to the last macro in the chain.
		
		function init()
		{
			// Temp. If neither setting is checked, don't do anything.
			if (!settings.quiz && !settings.questions)
			{
				$.debugLogSuccess("main-macro.js", "No settings checkboxes checked. No action taken.")
				return;
			}

			// Temp. Skip macros for testing.

			// First off, ensure editing is on.
			m = MEditingOn.run();

			// If a quiz needs to be created, do the following.
			if (settings.quiz)
			{
				m = m.runMacro(MAddQuiz).runMacro(MFillQuiz);
			}
		}

		init();
	};
});