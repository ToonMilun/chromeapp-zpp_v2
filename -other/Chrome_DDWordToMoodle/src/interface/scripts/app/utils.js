/**
 * Common utilities.
 */

define([], function(){

	return {

		debugLogError: function(text){

			console.log("%c " + text, "color: red;");
		},

		debugLogDebug: function(text){

			console.log("%c " + text, "color: magenta;");
		},

		debugLogSuccess: function(text){

			console.log("%c " + text, "color: green;");
		},

		setChromeData: function(dataObj, callback){
			chrome.storage.local.set(dataObj, callback);
		},

		/**
		 * Get data stored to the extension.
		 * @param {*} dataName 
		 * @param {*} callback 
		 */
		getChromeData: function(dataName, callback){
			chrome.storage.local.get(dataName, function(data){
				if (callback) callback(data[dataName]);
			});
		}
	};
});