/**
 * 1. Determine if editing is already one.
 * 	  - If it is, do nothing. End of macro.
 * 	  - If it isn't, click it. Then, wait for the page to finish reloading. End of macro.
 */

define(["macro"], function(Macro){

	return Macro.Macro.extend({

		onRun: function(d){

			$.debugLogHeader("editing-on", "Begin macro.");

			// 1.

			// Look for the "Turn editing off" button first. If it exists, editing is on, so no further action needed.
			var $editingOff = $w("#page-navbar input[value='Turn editing off']").first();
			if ($editingOff.exists())
			{
				$.debugLogSuccess("editing-on", "Detected that \"Turn editing on\" is enabled. Macro skipped.");
				d.resolve();
				return;
			}

			var $editingOn = $w("#page-navbar input[value='Turn editing on']").first();
			if (!$editingOn.exists())
			{
				$.debugLogError("editing-on", 'Could not find "Turn editing off" or "Turn editing on" button. Did you start this scipt while on the root page of your unit?');
			}

			$.debugLog("editing-on", "Turning editing on.");

			$editingOn[0].click();

			// Resolve
			$.oneParentWindowChanged(function(){

				// Moodle's JavaScript will generate some HTML after toggling to edit mode. Wait for it before proceeding. 
				$w("body").oneObserver(function(){
					d.resolve();
				});
			});
		}	
	});
});
