/**
 * Base class for all Macros. When executed, performs some action(s) and calls a callback if those actions are verified to be successful.
 * Allows for chaining macros (each macro will wait for the previous one to finish). Example:
 * var lastMacroInChain = Macro.runMacro(..., ...).runMacro(..., ...);
 */

define([], function(){

	var Macro = {

		deferred: null,

		// Override this function.
		onRun: null,

		extend: function(override){

			var e = $.extend({}, this, override || {}); // Used a deep copy (not sure if it needs to be deep just yet).
			e.deferred = $.Deferred();					// Give the new copy a fresh $.Deferred object.

			return e;
		},

		// Needs to be called manually.
		run: function()
		{
			console.log("Macro.run();");

			if (this.onRun)
			{
				// The onRun function needs to resolve the passed deferred object when it's ready.
				this.onRun(this.deferred);
			}
			else
			{
				// nullMacro.
				this.deferred.resolve();
			}

			return this;
		},

		// Chain.
		runMacro: function(nextMacro){

			$.when(this.deferred).then(function(){
				nextMacro.run();
			});

			return nextMacro;
		}
	}

	return {
		Macro: Macro
	}

	return {

		nullMacro: new Macro(), // Blank Macro. Any runMacro calls called from it will instanly run.

		/**
		 * 
		 * @param {*} macroFunc 
		 * @param {function(deferred)} successConditionFunc // The deferred is passed to it for resolving.
		 */
		runMacro: function(macroFunc, successConditionFunc)
		{
			var m = new Macro(macroFunc, successConditionFunc);
			m.run(); // Auto start the first Macro in the chain.

			return m;
		}
	}
});