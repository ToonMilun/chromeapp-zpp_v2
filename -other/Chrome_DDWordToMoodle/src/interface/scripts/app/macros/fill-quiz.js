/**
 * 1. Expand all accordions.
 * 2. Fill out information throughout the <input> elements.
 */

define(["macro"], function(Macro){

	return Macro.Macro.extend({

		onRun: function(d){

			$.debugLogHeader("fill-quiz", "Begin macro.");

			// 1.
			$w("fieldset.collapsed legend > a").each(function(){
				$(this)[0].click();
			});

			// 2.
			$.debugLog("fill-quiz", "Filling out inputs.");

			// Name.
			$w("#id_name").macroSetVal("??????????");

			/****/

			// Grade to pass.
			$w("#id_gradepass").macroSetVal("??????????");

			// Attempts allowed.
			$w("#id_attempts").macroSetVal("4");

			// Attempts allowed.
			$w("#id_grademethod").macroSetVal("1");

			/****/

			// New page.
			$w("#id_questionsperpage").macroSetVal("0");

			/****/

			// Click show more.
			var $fieldSets = $w("fieldset legend > a");
			var moreLessFound = false;
			$fieldSets.each(function(){
				if ($(this).text() == "Question behaviour")
				{
					var $ml = $(this).closest("fieldset").find(".moreless-toggler:not(.moreless-less)");
					if ($ml.exists()) $ml[0].click();
					moreLessFound = true;

					return false;
				}
			});
			if (!moreLessFound) $.debugLogError("fill-quiz", "Could not find \"Question behaviour\ accordion.");

			// Shuffle within questions
			$w("#id_shuffleanswers").macroSetVal("1");

			// How questions behave
			$w("#id_preferredbehaviour").macroSetVal("deferredfeedback");

			// Each attempt builds on the last
			$w("#id_attemptonlast").macroSetVal("1");

			/****/

			// Right answer
			$w("#id_rightanswerimmediately").macroSetChecked(false);

			// Right answer
			$w("#id_rightansweropen").macroSetChecked(false);

			// Right answer
			$w("#id_rightanswerclosed").macroSetChecked(false);

			/****/

			// Require Didasko reattempt rules
			$w("#id_didaskoquizaccessrule").macroSetVal("1");

			/****/

			// Completion tracking
			$w("#id_completion").macroSetVal("2");

			// Require passing grade.
			// This checkbox is meant to become enabled when Completion tracking is set, but it doesn't seemto be affected by it when it's changed by the macro.
			// So, instead I've set it to manually remove its "disabled" attribute to allow the value to be set.
			$w("#id_completionpass").removeAttr("disabled");
			$w("#id_completionpass").macroSetChecked(true);

			/****/

			$.debugLogSuccess("fill-quiz", "Filling inputs complete.");
		}	
	});
});
