/**
 * 1. Find the "Assess" section.
 * 2. Click "+ Add an activity or resource".
 * 3. Select "Quiz" after the window appears.
 * 4. Click "Add" in the window.
 * 5. Wait until the page has finished loading.
 * 6. End of macro.
 */

define(["macro"], function(Macro){

	return Macro.Macro.extend({

		onRun: function(d){

			$.debugLogHeader("add-quiz", "Begin macro.");

			// 1.
			var $subheadingHero = $w(".subheading-hero");
			var foundSubheading = false;
			for (var i = 0; i < $subheadingHero.length; i++)
			{
				if ($subheadingHero.eq(i).text() == "Assess")
				{
					$subheadingHero = $subheadingHero.eq(i);
					foundSubheading = true;
					break;
				}
			}
			if (!foundSubheading) $.debugLogError("add-quiz", "Could not find \"Assess\" section.");
			var $section = $subheadingHero.closest(".section.main");
			
			// Wait for the button to appear (seems to be added by JavaScript slightly after pageload).
			//$section.oneObserver(function(){
				// 2.
				var $addBtn = $section.find(".addresourcemodchooser").find("a").first();
				$addBtn.macroClick("add-quiz", "+ Add an activity or resource", "in \"Assets\" section.");

				// 3.
				//$w("body").oneObserver(function(){ // Wait for the window to appear.

					var $quizRadio = $w(".moodle-dialogue #module_quiz");
					$quizRadio.macroClick("add-quiz", "Quiz");
					
					// 4.
					var $submitBtn = $w(".moodle-dialogue .submitbuttons .submitbutton");
					$submitBtn.macroClick("add-quiz", "Add");

					// 5.
					$.oneParentWindowChanged(function(){

						// Wait for Moodle JavaScript to update the HTML before proceeding.
						$w("body").oneObserver(function(){
							// 6.
							d.resolve();
						});
					});
				//})
			//});
		}	
	});
});
