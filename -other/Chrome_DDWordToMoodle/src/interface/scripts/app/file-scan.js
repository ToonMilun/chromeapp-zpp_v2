/**
 * Gets the settings from the Interface and runs all applicable macros on the main page.
 */

define([], function(){

	const TABLE_SELECTOR = "table.MsoTableGrid";

	return function($iframe){

		var $results;

		// Easy selection from the uploaded .htm file.
		function $i(selector)
		{
			return $(selector, $iframe.contents());
		}

		function init()
		{
			$.debugLogDebug("Scanning file: " + $iframe.attr("src"));
		
			$results = $(".file-scan-results-wrapper");

			findQuizzes();
		}

		/**
		 * Find all "Question X" entries in the file (they will be added in as Quizzes in Moodle).
		 */
		function findQuizzes()
		{
			// 1. Find all tables titled "Question X Marking Guide"
			// 2. Create a selector out of all tables that come prev() to those.
			// 3. Create objects from the elements.

			// 1.
			var $nextTables = $i(TABLE_SELECTOR);
			$nextTables = $nextTables.filter(function(){
				var $firstCell = $(this).find("tr:first-child td:first-child");

				// The "Question X" table we're after always comes before the table with the header "Question X Marking Guide".
				if ($firstCell.text().toLowerCase().indexOf("marking guide") < 0) return false;
				if ($firstCell.text().toLowerCase().indexOf("question ") < 0) return false;

				return true;
			});

			// 2.
			var $tables = $();
			$nextTables.each(function(){
				$tables = $tables.add($(this).prevAll(TABLE_SELECTOR).first());
			});

			// 3.
			$tables.each(function(i, e){
				createQuiz($(this), i+1);
			});
		}

		function createQuiz($quizTable, quizNum)
		{
			// All settings which will need to be changed on Moodle. Other settings can remain as default.
			var q = {
				qnName: "",
				qnHTML: "",
				defaultMark: 1,
				resHtml: "",
				gradHtml: ""
			}

			// Question name*
			var qNum = "" + quizNum;
			if (qNum.length === 0) $.debugLogError("file-scan", "quizNum cannot be a zero-length string.");
			else if (qNum.length === 1) qNum = "0" + qNum; // Add a 0 to the start of the string (so that "1" becomes "01").
			q.qnName = "BSA_Q" + qNum;

			// Question text*
			// Everything that is inside of the second <tr>'s <td>.
			q.qnHTML = $quizTable.find("tr:nth-child(2) td").first().html();

			// Response template
			// Everything beyond the second $quizTable row.

			// Information for graders
		}

		init();
	};
});