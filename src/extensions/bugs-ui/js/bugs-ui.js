define([
    "zpp",
    "./bugsUIView",
    "./bugsUIModel"
  ], function(ZPP, BugsUIView, BugsUIModel) {

    /**
     * Bugs UI
     * --------------------------
     * 
     * When activated on a "bugsview" location, creates a popup window which automates bug submission.
     * Allows for MUCH faster assignment of bugs when bugging a topic with many Screens.
     */

    const LOCATION = "bugsview";      // Only activate if in this router location.
    const WINDOW_TITLE = "[DD] Zoho++ Bug Submitter";
    const WINDOW_WIDTH = 465;
    const WINDOW_HEIGHT = 580;

    // Selectors
    // ---------------
    // Defined here in case Zoho changes their selectors (which it tends to do).
    const _container = "#fixedtop_bugtracker"; // Container for the button.

    class BugsUIBtnView extends Backbone.View {

        className() {
            return "zpp-extra zpp-bugsui"
        }

        events() {
            return {
                "click .zpp-bugsui__btn" : "onClick"
            }
        }

        initialize() {
            this.render();
        }

        setUpEventListeners(){
            
        }

        render() {
            this.$el.append($("<div class='zpp-bugsui__inner'>"));
            this.$(".zpp-bugsui__inner").append($("<button class='zpp-btn zpp-bugsui__btn'>Submit bugs</button>"));
        }

        async onClick() {

            // Create the bugsUI model.
            let model = new BugsUIModel();
            if (model.get("_hasError")) {
                alert("Zoho++ has encountered an unexpected error while initializing the bug submission UI. Please follow any instructions that were provided in the previous popup(s), refresh the page, and try again.\n\nIf problems persist, please contact Milton Plotkin and describe the issue.");
                return;
            }

            // Wait for the model to be fully ready before proceeding.
            await model._awaitReady();

            // Create a new window / get the existing one and populate it with a new BugsUIView.
            await this.createPopupWindow();

            // Check if there's already a PopupWindow with the BugsUIView open.
            if (this.popupWindow.__exists) {
                alert("A Zoho++ bug submission window is already open.");
                return; // PopupWindow already exists.
            }
            
            // If the Zoho tab is closed / URL changed, then take the popup window with it.
            window.onunload = function() {
                this.popupWindow.close();
            }

            // Give a warning before changing URLs.
            window.addEventListener("beforeunload", function (e) {
                var confirmationMessage = "If you close this tab / go to another URL, the Zoho++ bug submission UI will close, and any unsaved changes will be lost.\n\nAre you sure?";
        
                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
            });

            let view = new BugsUIView({model: model});
            view.popupWindow = this.popupWindow;
            view.LOCATION = LOCATION;
            $("body", this.popupWindow.document).append(view.$el);
        }

        /**
         * Creates a popup window for the BugsUIView to sit in.
         */
        createPopupWindow()
		{
            return new Promise(resolve => {
                var popupWindow = window.open("", "zpp-bugsui-window", "width=" + WINDOW_WIDTH + ",height=" + WINDOW_HEIGHT + ",top=0,left=0", true);
                this.popupWindow = popupWindow;

                // Determine if the window has already been populated, and stop if true.
                if ($(".bugsui", popupWindow.document).length)
                {
                    popupWindow.document.close();
                    this.popupWindow.__exists = true;
                    ZPP.log.error("Prevented rendering of BugsUIView: an existing bugs-ui interface window already exists!");
                    resolve();
                    return;
                }

                ////////////////
                // NEW WINDOW //
                ////////////////

                popupWindow.document.title = WINDOW_TITLE;
                
                // Append a stylesheet.
                popupWindow.document.write('<link href="' + chrome.extension.getURL("src/extensions/bugs-ui/less/styles.css") + '" type="text/css" rel="stylesheet">'); 
                // JS scripts.
                /*popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/global/lib/jquery-2.1.4.min.js") + '"></script>');
                popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/global/lib/require/require.js") + '"></script>');
                popupWindow.document.write('<script type="text/javascript" charset="utf-8" src="' + chrome.extension.getURL("src/interface/scripts/app.js") + '"></script>');*/
                // Allow the window html to be edited now.
                popupWindow.document.close();
                
                // Wait for the base HTML to be ready before continuing.
                // Must be done with plain Javascript (otherwise, the event is sometimes at the wrong time).
                // https://stackoverflow.com/questions/4842432/jquery-receive-document-ready-on-child-window
                popupWindow.onload = function(){
                    var $window = $(popupWindow.document.body);
                
                    // Load the Chrome apps stored HTML file into the popup (see: background.js).
                    /*chrome.storage.local.get('interfaceHTMLData', function(data){
                        $window.html(data.interfaceHTMLData); // Populate the body with the interface.html data.
                        windowOnLoad();
                        windowOnCreate();
                    });*/

                    resolve();
                };
            });
		}
    }

    class BugsUISingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.
            
            $(_container).find(".zpp-bugsui").remove();
            $(_container).append(new BugsUIBtnView({model: this}).el);
        }
    }

    let bugsUI = new BugsUISingleton();
    return bugsUI;
});