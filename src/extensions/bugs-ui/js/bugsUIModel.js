define([
	"zpp"
], function(ZPP) {

	const _clickAnywhere = "#projectcontent";

	const _assigneeColHeader = '.mlt-header-name[column-name^="ASSIGNEE"]'; // Selector for the "ASSIGNEE" column header.
	const _bugRow = ".projindrow";	// Selector for any bug row.

	const _assigneeBtn = ".zps-editable-cell";	// Selector for the button inside the assignee cell.
	const _popup = "#zpsPopDefault"; // Popup which contains the list of users.

	const _popupUser = ".popuplist"; 		// Element containing the user name.
	const _popupUserAttr = "data-tooltip"; 	// Attribute belonging to the above that contains the user name

	class BugsUIModel extends Backbone.Model {
		defaults() {
			return {
				unitode: "#ERR",   // Example: IT08
				topicNum: 1,
				taskNum: 1,
				taskSuffix: "",

				bugType: "",        // SME | Writer | ID | etc.
				bugAssignee: "",    // Name

				_assignees: [],		// Array assignees derrived from the assignee dropdown in Zoho.

				bugTitle: "",       // Generated using the above.
				bugDescription: "", // Large block of HTML text describing the bug.

				_hasError: false,	// If something goes wrong in the initial setup, then this will be set to true.
				_isReady: false
			}
		}

		async initialize() {
			ZPP.lockZoho();
			await this.setUpAssignees();
			ZPP.unlockZoho();

			this.setUpUnitCode();
			this.set("_isReady", true);
		}

		_awaitReady(){
			return new Promise(resolve => {
				if (this.get("_isReady")) {
					resolve();
					return;
				}

				this.listenToOnce(this, "change:_isReady", resolve);
			});
		}

		/**
		 * Run a quick macro on Zoho to get the full list of users that can be assigned bugs.
		 */
		setUpAssignees() {
			return new Promise(resolve => {

				// Find which column the assignees are kept in in the bug table.
				let assigneeIdx = $(_assigneeColHeader).closest("th").index();

				// Next, find a cell which has the assignee dropdown button in it.
				let $bugRow = $(_bugRow).first();
				let $assigneeCell = $bugRow.find("td:nth-child(" + (assigneeIdx+1) + ")");

				if (!$assigneeCell.length) {
					alert("Zoho++ could not find the list of assignees (users) to populate the bug user interface.\n\nPlease ensure that the \"ASSIGNEE\" column is visible in Zoho's bug submission page, refresh, and try again");
					this.setError();
					resolve();
					return;
				}

				// Click on the cell to show the list of user names.
				$assigneeCell.find(_assigneeBtn)[0].click();

				// Wait for a mutation.
				ZPP.onLastMutation($(_popup)[0], () => {

					// Get all usernames.
					let usernames = [];

					$(_popup).find(_popupUser).each((i, e) => {
						usernames.push($(e).attr(_popupUserAttr));
					});

					// Ensure there are no duplicates.
					usernames = _.uniq(usernames);
					this.set("_assignees", usernames);

					// Hide the popup window (by clearing it).
					$(_popup).html("");

					resolve();
				});
			});
		}

		setError(hasError){
			this.set("_hasError", hasError === undefined ? true : hasError);
		}

		/**
		 * Determine the unit code from the window title.
		 * Usual document.title format example:
		 * 
		 * (50) [IT08] CSE1PGX Programming environment
		 * 
		 * The unit code is within the [].
		 */
		setUpUnitCode() {
			let title = document.title;
			let unitCode = title.match(/[[](.+?)[\]]/);

			// The unit code couldn't be automatically determined. Prompt the user to enter the input themselves.
			if (!unitCode || unitCode.length < 2) {
				unitCode = prompt("Zoho++ could not automatically determine the unit code for this page. This can be rectified my ensuring the project title follows the following format:\n\n[IT08] CSE1PGX Programming environment\n\nIn the meantime, please enter the unit code manually.");
			}
			
			if (unitCode) {
				this.set("unitCode", unitCode[1]);
			}
		}
	}

	return BugsUIModel;
});