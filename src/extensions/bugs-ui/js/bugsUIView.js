define([
    "zpp"
  ], function(ZPP) {

    let _submitBugsBtn = "#fileabugnew";    // Zoho "Submit Bugs" button.
    
    let _form = "#zps-rightformslide";      // Zoho form for filling out bugs.
    let _inputBugsTitle = "input#subject";  // Bugs title input.

    let _inputBugsDesc = "#zbugeditordiv iframe.ze_area";  // Bugs description iframe.
    let _bugsDescBody = ".ze_body";                        // Selector for the body element inside of the above iframe element.

    let _inputBugOwner = "#bug_owner_zpuid-container";    // Button to click to trigger the list of users to appear.
    let _bugOwnerPopup = "#bug_owner_zpuid-listbox";       // Element containing the list of users that appears when the above is clicked.
    let _bugOwnerPopupSearch = "#zbug_owner_zpuid-listbox-searchbox"; // Element containing a search box (needs to be typed into to make user names appear).
    let _bugOwnerItem = ".zdropdownlist__item";
    let _bugOwnerItemAttr = "aria-label";           // Attribute for each item which will be checked for matching the BugsUIView's dropdown list values.

    let _invalidAssigneeVal = "Unassigned";            // This value cannot be chosen from the Assignee dropdown.

    let _addBugBtn = ".slideFormBottombtn .zps-primary-button"; // Click on this inside the form to submit the bug.

    // Check to ensure all values entered correctly.
    let _checkViewClose = "#detailPageClose"; // Close this window before submitting bugs (leads to more accurate checking).
    let _checkView = "#zpsDetailView"; // Large popup element which appears after the bug is submitted (and contains all of the _check elements).
    let _checkBugTitle = "#bugtitleStatic";
    //let _checkBugDescIframe = "#iframeDescrp"; // DEFUNCT
    let _checkBugDesc = "#compDescriptionEditor";
    let _checkBugAssignee = ".dt-field-value[data-zqpa=\"ASSIGNEE_ZPUID\"]";

    class BugsUIView extends Backbone.View {
        
        className() {
            return "bugsui";
        }

        events() {
            return {
                "change .js-bugsui-unitcode"    : "onUnitCodeChange",
                "keyup .js-bugsui-unitcode"    : "onUnitCodeChange",
                "change .js-bugsui-topicnum"    : "onTopicNumChange",
                "change .js-bugsui-tasknum"     : "onTaskNumChange",
                "change .js-bugsui-tasksuffix"  : "onTaskSuffixChange",
                "keyup .js-bugsui-tasksuffix"   : "onTaskSuffixChange",
                
                "change .js-bugsui-bugtype"     : "onBugTypeChange",
                "change .js-bugsui-bugassignee" : "onBugAssigneeChange",

                "click .js-bugsui-submit"       : "onSubmitClick",
                "click .js-bugsui-blocker-toggle" : "toggleBlocker"
            }
        }

        async initialize() {

            this.setUpEventListeners();
            await this.render();

            // Set up initial values and triggers.
            this.model.set("bugAssignee", _invalidAssigneeVal);
            this.onTitlePropChange();

            // Set up initial width of the unitCode input.
            this.updateUnitCodeWidth();
        }

        render() {
            //this.$el.html("<h2>TESTING</h2>");
            return new Promise(resolve => {
                ZPP.getChromeData("templates", (templates) => {
                    this.$el.html(Handlebars.compile(templates["bugs-ui"])(this.model.toJSON()));
                    resolve();
                });
            });
        }

        setUpEventListeners() {
            this.listenTo(this.model, {
                "change:unitCode": this.onTitlePropChange,
                "change:topicNum": this.onTitlePropChange,
                "change:taskNum": this.onTitlePropChange,
                "change:taskSuffix": this.onTitlePropChange,
                "change:bugType": this.onTitlePropChange,
                //"change:bugAssignee": this.onAnyChange
            });

            this.listenTo(this.model, "change:bugTitle", this.onBugTitleChange);
        }

        // Update the generated bug title.
        onTitlePropChange() {

            let title = "" + this.model.get("unitCode") + "-" + this.model.get("topicNum") + "-" + this.model.get("taskNum") + this.model.get("taskSuffix") + " (" + (this.model.get("bugType") || "???") + " bug)";
            this.model.set("bugTitle", title);
        }

        onBugTitleChange(model, bugTitle) {
            this.$(".js-bugsui-bugtitle").text(bugTitle);
        }

        onUnitCodeChange(event) {
            this.model.set("unitCode", $(event.currentTarget).val());
            this.updateUnitCodeWidth();
        }

        updateUnitCodeWidth() {
            // Adjust width of input to fit (FIXME: rough estimate).
            this.$(".js-bugsui-unitcode").css("width", (this.model.get("unitCode").length * 0.65) + "em");
        }

        onTopicNumChange(event) {
            this.model.set("topicNum", $(event.currentTarget).val());
        }

        onTaskNumChange(event) {
            this.model.set("taskNum", $(event.currentTarget).val());
        }

        onTaskSuffixChange(event) {
            this.model.set("taskSuffix", $(event.currentTarget).val());
        }

        onBugTypeChange(event) {
            this.model.set("bugType", event.currentTarget.value);
        }

        onBugAssigneeChange(event) {
            this.model.set("bugAssignee", event.currentTarget.value);
        }

        lock() {
            this.$(".bugsui__blocker").show();
        }

        unlock() {
            this.$(".bugsui__blocker").hide();
        }

        toggleBlocker() {
            this.$(".bugsui__blocker").toggle();
        }

        // Show an alert belonging to this View's popupWindow.
        alert(msg) {
            this.popupWindow.alert(msg);
        }

        /**
         * Log a message to the console inside the blocker (note: won't be visible unless the macro is running).
         * @param {*} msg 
         */
        log(msg) {
            let $console = this.$(".bugsui__blocker-console");
            $console.append($("<p>" + (msg || "--------------------") + "</p>"));
            $console[0].scrollTop = $console[0].scrollHeight; // Always scroll the latest entry into view.
        }

        logError(msg){
            this.log("<span style='color: #AA0000;'>" + msg + "</span>");
            this.alert(msg + "\n\nPlease save the bug you were currently working on, and then reload the Zoho page to try again. If this problem persists, please contact Milton Plotkin.");

            this.unlockZoho();
        }

        triggerMouseEvent(el, eventType) {
            var clickEvent = document.createEvent('MouseEvents');
		    clickEvent.initEvent(eventType, true, true);
		    el.dispatchEvent(clickEvent);
        }

        async onSubmitClick() {

            // Check if the URL is in the right location.
            if (!ZPP.matchRouterLocation(this.LOCATION) && !ZPP.matchRouterLocation("buginfo")) {
                this.alert("The Zoho++ bug submitter can only submit bugs if the Zoho tab it's tethered to is on a \"Bugs\" page for a project. Please navigate back to the \"Bugs\" page of your project, and try submitting again.\n\nFor optimal performance, it is recommended you DO NOT navigate away from the Zoho bugs page after opening this bug submitter (you are more than welcome to have other Zoho pages open in other Chrome tabs if you need to though).");
                return;
            }

            // Check that the Assignee has been set to a non-"Unassigned" value.
            if (this.model.get("bugAssignee").toLowerCase().trim() == _invalidAssigneeVal.toLowerCase().trim()) {
                this.alert("Please set a valid \"Assignee\" value (it cannot == " + _invalidAssigneeVal + ").");
                return;
            }

            // Store the bug description.
            // ------------------------------------------
            let bugDescription = this.$(".js-bugsui-desc").html();
            let bugDescriptionText = this.$(".js-bugsui-desc").text();

            // Check that the description has text in it.
            if (!bugDescriptionText.trim()) {
                this.alert("Please enter a bug description.");
                return;
            }

            this.lock();
            ZPP.lockZoho();

            // Close the description window (if any is present).
            let $detailsCloseBtn = $(_checkViewClose);
            if ($detailsCloseBtn.length) $detailsCloseBtn[0].click();

            // Click on the "Submit Bugs" button.
            // ------------------------------------------
            this.log("Clicking Zoho's \"Submit Bugs\" button.");
            $(_submitBugsBtn)[0].click();
            this.log("Waiting for bug form mutation...");
            await this._awaitBugFormMutation();

            // Fill out the form.
            // ------------------------------------------
            let $form = $(_form);

            // Bug title
            this.log("Setting bug title.");
            $form.find(_inputBugsTitle).val(this.model.get("bugTitle"));

            // Bug desc
            // Populate the iframe.
            this.log("Setting bug description.");
            let $descIframe = $(_inputBugsDesc).first();
            let $descBody = $(_bugsDescBody, $descIframe.contents());
            // Fallback
            const ATTEMPTS = 3;
            const SLEEP = 250;
            let attempts = ATTEMPTS;
            while (!$descBody.length && attempts > 0) {
                this.log("Could not set bug description! Attempting again in " + SLEEP + "ms...");
                ZPP.sleep(SLEEP);
                attempts--;
                $descBody = $(_bugsDescBody, $descIframe.contents());
            }
            if (!$descBody.length) {
                this.logError("An unexpected error has occurred: could not populate bug description body iframe after " + ATTEMPTS + " attempts.");
                return;
            }
            $descBody.html(bugDescription);
            this.log("Bug description set successfully.");
            
            // Bug assignee
            // (For whatever reason, this specific element is triggered with mousedown in Zoho).
            // NEW:
            // Due to a new update to Zoho, sometimes the user popup will disappear immediately.
            // Attempt to perform this task 10 times as a fallback.
            let assigneeSetAttempts = 0;
            while (assigneeSetAttempts < 10) {
                assigneeSetAttempts++;

                this.log("Clicking \"Assignee\" button (attempt #" + assigneeSetAttempts + ")...");
                this.triggerMouseEvent($form.find(_inputBugOwner)[0], "mousedown");

                await this._awaitBugOwnerPopupMutation();

                // Type in the first two letters of the user's name into the input box.
                // This will ensure that the user's name appears in the dropdown (Zoho only shows X usernames at a time).
                $(_bugOwnerPopupSearch).val(this.model.get("bugAssignee"));

                await ZPP.sleep(100);

                // Trigger the event that will make Zoho filter the dropdown list.
                $(_bugOwnerPopupSearch)[0].dispatchEvent(new Event('focus'));
                $(_bugOwnerPopupSearch)[0].dispatchEvent(new Event('input'));

                await ZPP.sleep(100); // Just in case.

                this.log("Setting bug assignee.");
                // Click on the bugOwnerItem that matches what was set in the UI.
                let $bugOwnerItem = $(_bugOwnerPopup).find(_bugOwnerItem).filter("[" + _bugOwnerItemAttr + "=\"" + this.model.get("bugAssignee") + "\"]");

                // (For whatever reason, this specific element is triggered with mouseup in Zoho).
                // This will throw an error if the popup window isn't active when called (at which point, attempt again).
                try {
                    this.triggerMouseEvent($bugOwnerItem[0], "mouseup");
                    break;
                }
                catch (err) {
                    
                }
            }

            this.log("All bug data entered into form successfully!");

            await ZPP.sleep(100); // Just in case.
            
            this.log("Clicking \"Add\" button...");

            // Click submit.
            // ------------------------------------------
            $(_addBugBtn)[0].click();
            
            // Check that all the bug details were entered correctly.
            // ------------------------------------------

            this.log("Checking that all information was submitted to Zoho correctly...");

            await this._awaitCheckViewMutation();
            await ZPP.sleep(100); // Just in case.

            let $checkView = $(_checkView);
            if ($checkView.find(_checkBugTitle).text().trim() !== this.model.get("bugTitle").trim()) {
                this.logError("Bug submission title does not match what was entered in UI.");
                return;
            }
            
            // If the text added to the description is drastically different in length to what was entered in the bug UI,
            // assume something went wrong.
            let bugDescCompare = $checkView.find(_checkBugDesc).text().length / bugDescriptionText.trim().length;
            if (bugDescCompare < 0.8) {
                this.logError("Bug submission description does not match what was entered in UI.");
                return;
            }
            if ($checkView.find(_checkBugAssignee).text().trim() !== this.model.get("bugAssignee").trim()) {
                this.logError("Bug submission assignee does not match what was entered in UI.");
                return;
            }

            this.log("Done!");

            this.log("");

            // Move the bugAssignee's <option> element to the top of the <select> list (for convenience).
            let $bugAssigneeSelect = this.$(".js-bugsui-bugassignee");
            $bugAssigneeSelect.find("option[value=\"" + this.model.get("bugAssignee") + "\"]").addClass("is-visited").prependTo($bugAssigneeSelect);

            // Clear the UI's bug description for the next bug.
            this.$(".js-bugsui-desc").html("<ol><li></li></ol>");

            ZPP.unlockZoho();
            this.unlock();
        }

        _awaitBugFormMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_form)[0], async() => {

                    // Check if the bug description iframe is ready. If not, wait for another mutation.
                    if (!$(_inputBugsDesc).length) {
                        await this._awaitBugFormMutation();
                        resolve();
                        return;
                    }

                    resolve();
                }, 500);
            });
        }

        _awaitBugOwnerPopupMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_bugOwnerPopup)[0], resolve, 500);
            });
        }
        
        async _awaitCheckViewMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_checkView)[0], async () => {

                    // If the element is empty when the mutation triggers, wait for another one.
                    if (!$(_checkView).children().length) {
                        await this._awaitCheckViewMutation();
                        resolve();
                        return;
                    }

                    resolve();
                }, 500);
            });
        }

        submitBug() {
        }
    }

    return BugsUIView;
});