define([
    "zpp"
  ], function(ZPP) {

    /**
     * Project Groups
     * --------------
     * 
     * Organises all projects in the "allprojects" router location to DD requirements.
     */

    const LOCATION = "allprojects"; // Only activate if in this router location.

    // Selectors
    // ---------------
    // Defined here in case Zoho changes their selectors (which it tends to do).
    const _container = ".left-action-container";    // Selector for the container for the button.
    const _projectsTable = ".table-group-list";     // Selector for the parent table of the project rows.
    const _projectRow = ".projindrow";              // Selector for each project row.
    const _cellGroup = ".mlt-item-group";           // Selector for cell containing the project's group name.

    const _customViewHolder = "#projectviewholder .cvbtn";

    const _sortBtn = ".sortoption[column-name='PROJGROUPNAME']";    // Button to click on to sort the projects by group name.

    const _projectContent = "#projectcontent";      // Main element (will update itself when the sort button is clicked).

    class ProjectGroupView extends Backbone.View {

        className() {
            return "zpp-extra zpp-projectgroup"
        }

        events() {
            return {
                "click .zpp-projectgroup__btn" : "onClick"
            }
        }

        initialize() {
            this.render();
        }

        setUpEventListeners(){
            $(".zpp-projectgroup__group-header").on("click", function() {
                
                let group = $(this).attr("data-zpp-group");
                $(".zpp-projectgroup__group-content").filter("[data-zpp-group='" + group + "']").toggle(); // FIXME: Temp.
            });
        }

        render() {
            this.$el.append($("<div class='zpp-projectgroup__inner'>"));
            this.$(".zpp-projectgroup__inner").append($("<button class='zpp-btn zpp-projectgroup__btn'>Sort into groups</button>"));
        }

        /**
         * The page must be in "All Tasks" view to give a accurate results. Ensure that it is.
         */
        checkCorrectZohoView(){
            return $(_customViewHolder).text().trim().toLowerCase() == "dd view";
        }

        /* Clear any previous instances of this View's activation (if any). */
        clearRender() {
            $(".zpp-projectgroup__group-header").removeClass("zpp-projectgroup__group-header");
        }

        onClick() {

            if (!this.checkCorrectZohoView()){
                alert("Please ensure that the Zoho view is set to the custom \"DD View\" to ensure Zoho++ will give accurate results.\n\nThe view selection dropdown is located in the top left of this page. You can set the view to be the default by clicking on the anchor when you mouse-over it.\n\nIf you do not have the \"DD View\" in the dropdown, please contact Milton Plotkin and he will share it with your account.");

                return;
            }

            this.renderSort();
        }

        _awaitProjectContentMutation() {
            return new Promise(resolve => {
                ZPP.onLastMutation($(_projectContent)[0], resolve);
            });
        }

        async renderSort() {

            this.clearRender();
            
            // Zoho's inbuilt sorting button MUST be used.
            // Sorting by rearranging the position of the row elements breaks click events.
            $(_sortBtn)[0].click();

            await this._awaitProjectContentMutation();

            let lastGroupName = "";
            $(_projectRow).each((i, e) => {
                let groupName = $(e).find(_cellGroup).first().text().trim();

                if (groupName != lastGroupName) {
                    lastGroupName = groupName;
                    $(e).addClass("zpp-projectgroup__group-header");
                }
            });

            return;

            this.clearRender();
            
            let groups = {}; // Dictionary of groups and the rows they contain.

            // Sort the rows by the groups they belong to.
            $(_projectRow).each((i, e) => {
                
                // FIXME:
                // FIXME:
                // FIXME:
                // The current _cellGroup selector is bad! Selects multiple elements per row.
                // The use of "first()" should be a temporary fix.
                let group = $(e).find(_cellGroup).first().text().trim();
                if (groups[group] === undefined) groups[group] = [];
                groups[group].push($(e));
            });
            
            // Create a row (accordion button) for each group, and add the corresponding rows after it.
            _.each(groups, (rows, key) => {
                $(_projectsTable).append("<tr colspan='3' data-zpp-group='" + key + "' class='zpp-extra zpp-projectgroup__group-header'><td>" + key + "</td></tr>");

                _.each(rows, ($row) => {
                    $(_projectsTable).append($row);
                    $row.attr("data-zpp-group", key).addClass("zpp-projectgroup__group-content").hide();
                });
            });

            this.setUpEventListeners();
            
            /*.sort(function(a,b) {

                let aGroup = $(a).find(_cellGroup).text().trim();
                let bGroup = $(b).find(_cellGroup).text().trim();
                return aGroup.localeCompare(bGroup);

           }).appendTo(_projectsTable);*/
        }
    }

    class ProjectGroupsSingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.

            // Fallback: clear any existing zpp-extras.
            $(".zpp-projectgroup").remove();
            $(_container).append(new ProjectGroupView({model: this}).el);
        }
    }

    let projectGroups = new ProjectGroupsSingleton();
    return projectGroups;
});