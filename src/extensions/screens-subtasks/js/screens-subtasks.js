define([
    "zpp"
  ], function(ZPP) {

    /**
     * Screens Subtasks Generator
     * --------------------------
     * 
     * Creates a button in the "Subtasks" tab of the "Screens" task of a Subject project.
     * When clicked, will add a number of XXXX-X-X screen subtasks. 
     */

    const LOCATION = "taskdetail";      // Only activate if in this router location.
    const VALID_TASKNAMES = [
        "screens",
        "style updates & re-development",
        "development"
    ];   // Only create the view if the task currently being viewed is called one of these elements.

    // Selectors
    // ---------------
    // Defined here in case Zoho changes their selectors (which it tends to do).
    const _container = "#taskdetsbtsk";             // Selector for the container for the button.
    
    const _titleArea = "#titleArea";                // The text in this element must be "Screens".
    const _addSubtaskBtn = "#taskdetsubtask";       // Existing Zoho button that will need to be programatically clicked on.
    const _tabContainer = "#tabContainer";          // The element containing the "Comments" | "Subtask" | "Log Hours" | "Documents" | Etc.
    const _tabContainerSubtask = "#taskhierarchy_tab"; // The "Subtask" tab in the above.
    const _subtaskList = "#taskdet_subtasktbody";   // Element where the subtasks are added into.

    const _form = "#zps-rightformslide";            // Slide-out form that Subtask info is added into.
    const _subtaskNameArea = "#todotask";           // Subtask names entered into here.
    const _subtaskAddSubtask = "#addtoptasksubmit"; // Button clicked automatically to add the subtask.

    class ScreensSubtasksView extends Backbone.View {

        className() {
            return "zpp-extra zpp-screenssubtasks"
        }

        events() {
            return {
                "click .zpp-screenssubtasks__btn" : "onClick"
            }
        }

        initialize() {
            this.render();
        }

        setUpEventListeners(){
            
        }

        render() {
            this.$el.append($("<div class='zpp-screenssubtasks__inner'>"));
            this.$(".zpp-screenssubtasks__inner").append($("<button class='zpp-btn zpp-screenssubtasks__btn'>Generate Screen Tasks</button>"));
        }

        /**
         * 
         */
        checkCorrectTask(){
            return $(_customViewHolder).text().trim().toLowerCase() == "dd view";
        }

        /* Clear any previous instances of this View's activation (if any). */
        /*clearRender() {
            $(".zpp-projectgroup__group-header").remove();
        }*/

        _awaitSubtaskFormMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_form)[0], resolve, 500);
            });
        }

        _awaitSubtaskListMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_subtaskList)[0], resolve, 0);
            });
        }

        async onClick() {

            // 1. Prompt
            // ----------------------------

            var subtaskStart = prompt("Start subtasks at which number?", "1");
            if (subtaskStart === false || subtaskStart === undefined) return;

            // Prompt the user for the number of subtasks.
            var subtaskCount = prompt("End subtasks at which number?", "10");
            if (!subtaskCount) return;

            
            
            // Prompt the user for subtask names. 
			var subtaskPrefix = prompt("Enter the Screen prefix. Example:\n\nBU11-8-", "??XX-X-");

            // Confirm.
			if (!confirm("Your subtask prefix is: '" + subtaskPrefix + "'. An example subtask created using this prefix would be called: '" + subtaskPrefix + "1'. Is this correct?")) return;

            // 2. Macro.
            // ----------------------------

            // Lock screen.
            ZPP.lockZoho();

            for (var i = parseInt(subtaskStart)-1; i < subtaskCount; i++) {

                // Click on the "Add Subtask" button.
                $(_addSubtaskBtn)[0].click();

                // Wait a bit (for the form to be ready).
                await this._awaitSubtaskFormMutation();

                // And just wait a bit more to be safe (the above has sometimes resolved too quickly for Zoho).
                await ZPP.sleep(250);

                // Fill in the title (inside the popup).
                $(_subtaskNameArea).val(subtaskPrefix + (i+1));
                
                // Click "Add Subtask" button (that's inside the popup).
                $(_subtaskAddSubtask)[0].click();

                // Wait for the subtask list to mutate before continuing.
                await this._awaitSubtaskListMutation();
            }

            // Unlock screen.
            ZPP.unlockZoho();
            

            /*if (!this.checkCorrectZohoView()){
                alert("Please ensure that the Zoho view is set to the custom \"DD View\" to ensure Zoho++ will give accurate results.\n\nThe view selection dropdown is located in the top left of this page. You can set the view to be the default by clicking on the anchor when you mouse-over it.\n\nIf you do not have the \"DD View\" in the dropdown, please contact Milton Plotkin and he will share it with your account.");

                return;
            }

            this.renderSort();*/
        }
    }

    class ScreensSubtasksSingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        /**
         * Only create the view if the task currently being viewed is called "Screens".
         */
        checkCorrectTask() {
            let title = $(_titleArea).val();
            try {
                //return title.trim().toLowerCase() == VALID_TASKNAME.trim().toLowerCase();

                let t = title.trim().toLowerCase();
                return _.find(VALID_TASKNAMES, (taskName) => {
                    return t == taskName.trim().toLowerCase();
                }) !== undefined;
            }
            catch (err) {
                return false;
            }
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.
            if (!this.checkCorrectTask()) return;

            // The bottom menu needs to be in the "Subtask" tab. Listen for mutations.
            ZPP.onMutation($(_tabContainer)[0], () => {
                // The "Subtask" tab was just activated; create the view.
                if ($(_tabContainerSubtask).hasClass("active")){

                    // Fallback: clear any existing zpp-extras.
                    $(_container).find(".zpp-extra").remove();
                    $(_container).append(new ScreensSubtasksView({model: this}).el);
                }
            }, 1000);
        }
    }

    let screensSubtasks = new ScreensSubtasksSingleton();
    return screensSubtasks;
});