define([
    "zpp"
  ], function(ZPP) {

    /**
     * Mile(stone)Stats
     * ----------------
     * 
     * Adds a button to the group header elements in "todomilestones" that will calculate and provide a summary of 
     * the group when clicked.
     */

    const LOCATION = "todomilestones"; // Only activate if in this router location.

    // Selectors
    // ---------------
    // Defined here in case Zoho changes their selectors (which it tends to do).
    const _commonTable = "#common_table";           // Selector for table containing all milestones.
    const _groupHeader = ".table-group-header";     // Selector for the <tbody> containing the header for each milestone.
    const _groupList = ".table-group-list";         // Selector for the <tbody> containing all the tasks for each milestone.
    const _container = ".group-title-holder";       // Stats element appended into here.
    const _customViewRender = "#customviewrender";

    const _row = "tr.fieldpermis";                  // Each row containing a task in the table.
    const _cellDuration = "td[id^=td_]";            // "Duration" table cells.
    const _cellComplete = "td[id^=tper_]";          // "% Completion" table cells (0% - 100%).
    const _cellCompleteProgressHolder = ".progress-holder"; // Element inside of the above which has a "name" attr which stores its value.

    const _loadMore = "[id^=seemoretasktr].mltload-link";        // "Load more" button (needs to be clicked before stats are calculated).

    /**
     * Basic model storing the data contained in a single row.
     */
    class RowModel extends Backbone.Model {
        defaults() {
            return {
                _depth: 0,          // Level of nesting     As seen in Zoho.
                _duration: 0,       // Duration (in days).  As seen in Zoho.
                _completion: 0,     // Percent completion (calculated based on child completion percents).  As seen in Zoho.
                _$duration: undefined,  // Element in Zoho containing the duration stats. 
                _$completion: undefined,// Element in Zoho containing the completion %.

                _trueDuration: 0,   // Custom DD calculation. Determined first; from top-level down.
                _trueCompletion: 0, // Custom DD calculation. Determined second; from bottom-level up.

                _parent: null,
                _children: null
            }
        }

        initialize(){
            this.set("_parent", null);
            this.set("_children", new Backbone.Collection());

            // Don't allow top-level tasks to have no duration.
            if (this.get("_depth") === 0 && !this.get("_duration")) {
                this.set("_trueDuration", 0.1);
            }
        }

        addChild(rowModel){
            this.get("_children").push(rowModel);
            rowModel.set("_parent", this);
        }

        /**
         * Calculate the rowModel's trueCompletion value based on its children. 
         */
        recalculate() {

            // Calculate the true duration for this RowModel and all of its children.
            this.calcTrueDuration();
            this.calcTrueCompletion();
        }

        calcTrueCompletion() {
            
            let children = this.get("_children");

            // If no children, then completion = that which is shown in Zoho.
            if (!children.length) {
                this.set("_trueCompletion", this.get("_completion"));
                return;
            }

            // Calculate for children first.
            children.each((child) => {child.calcTrueCompletion();});

            // Set _trueCompletion based on the child._completion * child._trueDuration values.
            let totalCompletion = 0;
            let trueCompletion = 0;
            children.each(child => {
                let childTrueDuration = child.get("_trueDuration");
                totalCompletion += childTrueDuration;
                trueCompletion += childTrueDuration * (child.get("_completion")/100);
            });

            let tc = (trueCompletion / totalCompletion) * 100;
            if (isNaN(tc)) tc = 0; // Divided by zero error.
            if (trueCompletion == 0 && totalCompletion == 0) tc = 100;
            this.set("_trueCompletion", tc);

            this.updateCompletionEl(tc);
        }

        // Update the appearance of the _$completion element to match the provided value.
        // (And turn it red).
        updateCompletionEl(completion) {
            let $completion = this.get("_$completion");

            $completion.find(".percentCount").html(
                "<span class='zpp-extra zpp-milestats__red'>" + 
                Math.round(completion) + 
                " %</span>"
            );

            $completion.find(".greenbar").css({
                width: completion + "%",
                background: "#d37c7c"
            });

            $completion.find(".redbar").css({
                width: "calc(100% - " + completion + "%)",
                background: "#e4d9d9"
            });
        }

        calcTrueDuration() {
            let duration = this.get("_duration") || this.get("_trueDuration"); //|| 0.1; // Don't let duration be 0.
            let durationDefined = this.get("_duration") != 0;
            let children = this.get("_children");

            let totalChildDuration = 0;
            let undefChildren = []; // The children with no duration (0) specified.
            children.each((child) => {
                let d = child.get("_duration");
                totalChildDuration += d;
                if (d == 0) undefChildren.push(child);
            });

            // There are children in this row with no duration specified. Give them a calculated one.
            if (undefChildren.length) {
                let undefChildDuration = (duration - totalChildDuration)/undefChildren.length;
                _.each(undefChildren, (child) => {
                    child.set("_trueDuration", undefChildDuration);
                });                
            }

            // If the duration for this RowModel was calculated, show the calculated value on the page.
            if (!durationDefined) {

                this.get("_$duration").html("<span class='zpp-extra zpp-milestats__red'>" + +parseFloat(this.get("_trueDuration")).toFixed(2) + " days</span>");
            }
            else {
                this.set("_trueDuration", duration);
            }

            children.each((child) => {
                child.calcTrueDuration();
            });
        }
    }

    class MileStatsView extends Backbone.View {

        className() {
            return "zpp-extra zpp-milestats"
        }

        events() {
            return {
                "click .zpp-milestats__btn" : "onClick"
            }
        }

        initialize() {
            this.render();
        }

        render() {
            this.$el.append($("<div class='zpp-milestats__inner'>"));
            this.$(".zpp-milestats__inner").append($("<button class='zpp-btn zpp-milestats__btn'>STATS</button>"));
            this.$(".zpp-milestats__inner").append($("<div class='zpp-milestats__content-container'></div>"));
            this.$(".zpp-milestats__content-container")
                .append($("<span title='Milestone completion (in estimated days).' class='zpp-milestats__content zpp-milestats__content-duration'></span>"));
            this.$(".zpp-milestats__content-container")
                .append($("<span title='Milestone completion percentage.' class='zpp-milestats__content zpp-milestats__content-completion'></span>"));
        }

        /**
         * The page must be in "All Tasks" view to give a accurate results. Ensure that it is.
         */
        checkCorrectZohoView(){
            return $(_customViewRender).text().trim().toLowerCase() == "all tasks";
        }

        _awaitGroupListMutation() {
            return new Promise(resolve => {
                ZPP.onLastMutation(this.$groupList[0], resolve);
            });
        }

        async onClick() {

            if (!this.checkCorrectZohoView()){
                alert("Please ensure that the Zoho view is set to \"All Tasks\" to ensure Zoho++ will give accurate results.\n\nThe view selection dropdown is located in the top left of this page. You can set the view to be the default by clicking on the anchor when you mouse-over it.");
            }

            ZPP.lockZoho();

            // Click on all "Load more" buttons for this Tasklist.
            while(true) {

                // Find the next "Load more" button.
                let $loadMore = this.$groupList.find(_loadMore);

                if (!$loadMore.length) break; // If there isn't one, stop the loop.

                // Click the button to show more content.
                $loadMore[0].click();

                // Wait for the content to be shown.
                await this._awaitGroupListMutation();
            }

            // Remove any elements added by renderGroupListStats first.
            $(".zpp-milestats__red").remove();

            // Get the stats for the $groupList and display them. 
            let stats = this.model.getGroupListStats(this.$groupList);
            
            this.$(".zpp-milestats__content-duration")
                .text("🕑 " + +parseFloat(stats.duration).toFixed(2) + " / " + +parseFloat(stats.totalDuration).toFixed(1));
            this.$(".zpp-milestats__content-completion")
                .text("🏁 " + stats.completion.toFixed(2) + "%");

            ZPP.unlockZoho();
        }
    }

    class MileStatsSingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.

            // Fallback: clear any existing milstats extras.
            $("[class*='zpp-milestats']").remove();

            let $headers = $(_groupHeader);
            if (!$headers.length) {
                ZPP.log.error("Could not find any _groupHeader elements. Selectors may need to be updated.");
                return;
            }
            // For each group header...
            $(_groupHeader).each((i, e) => {

                // Find the corresponding group list.
                let $groupHeader = $(e);
                let $groupList = $(e).next(_groupList);

                if (!$groupList.length) {
                    ZPP.log.error("Could not find any _groupList elements. Selectors may need to be updated.");
                    return;
                }

                // Append a button to the $groupHeader.
                let $container = $groupHeader.find(_container);
                let view = new MileStatsView({model: this});

                view.$groupList = $groupList;
                $container.append(view.$el);
            });
        }

        // Calculate the stats for the groupList.
        getGroupListStats($groupList) {

            let topRows = this.getTopRowModelCollection($groupList);
            let totalCompletion = 0;
            let totalDuration = 0;
            topRows.each((rowModel) => {
                totalCompletion += (rowModel.get("_trueCompletion") / 100) * rowModel.get("_trueDuration");
                totalDuration += rowModel.get("_trueDuration");
            });

            let completion = totalCompletion / totalDuration;

            return {
                completion: completion * 100,
                totalCompletion: totalCompletion,
                duration: totalDuration * completion,
                totalDuration: totalDuration
            }
        }

        // Create models based on the elements on the page.
        getTopRowModelCollection($groupList) {

            let rowArr = [];
            let topRowCollection = new Backbone.Collection();

            function getLastOfDepth(depth) {
                return _.last(_.filter(rowArr, (rowModel) => {
                    return rowModel.get("_depth") == depth;
                }));
            }

            $groupList.find(_row).each((i, e) => {
                let $row = $(e);

                // Depth
                // --------------------
                let depth = parseInt($row.attr("depth"), 10);
                
                // Duration
                // --------------------
                let $cellDuration = $row.find(_cellDuration);
                let dayString = $cellDuration.text().trim();
                let duration = dayString.match(/^\d+/g);
                duration = !duration ? 0 : parseInt(duration, 10);
                
                // % Completion
                // --------------------
                let $completion = $row.find(_cellComplete); 
                let $completionProgress = $completion.find(_cellCompleteProgressHolder);
                let completion = parseInt($completionProgress.attr("name"), 10);
                if (isNaN(completion)) completion = 100;

                // Create a new RowModel.
                let r = new RowModel({
                    _depth: depth,
                    _duration: duration,
                    _$duration: $cellDuration.find(".mlt-item-group"),
                    _$completion: $completionProgress,
                    _completion: completion
                });
                rowArr.push(r);

                // If this is a top-level model, record it.
                if (depth === 0) {
                    topRowCollection.push(r);
                }
                // If this isn't a top-level model, add it as a child to the one above it.
                else {
                    getLastOfDepth(depth-1).addChild(r);
                }
            });

            topRowCollection.each((rowModel) => {rowModel.recalculate()});
            return topRowCollection;
        }
    }

    let mileStats = new MileStatsSingleton();
    return mileStats;
});