define([
    "zpp"
  ], function(ZPP) {

    /**
     * Column view setup shortcut
     * --------------------------
     * 
     * Zoho doesn't let us customize the columns visible by default.
     * This will create a button which will automate it for convenience.
     */

    const LOCATION = "todomilestones";      // Only activate if in this router location.
    const COLUMNS = [ // In order of how we want them to appear.
        "owner",
        "status",
        "duration",
        "completion percentage",
        "[d] template",
        "[d] tag",
        "[d] qa status",
        "due date"
    ];

    // Selectors
    // ---------------
    // Defined here in case Zoho changes their selectors (which it tends to do).
    
    const _popup = "#zpsPopDefault";                // Popup which appears when the column customization button is clicked.
    const _container = "#zpsPopDefault #custcoltab";// Container for the button (placed inside the popup).

    const _columnContainer = "#zpsPopDefault .ui-sortable";        // Contains all columns (and they will be rearranged in it).
    const _column = ".customdashSort-list";
    const _columnSwitchInput = ".zp-switch input";

    const _saveContainer = "#fixsave";  // Container containing the save button. Needs to be programmatically shown.
    const _saveBtn = "#saveorder";      // Save btn.

    class ColumnsView extends Backbone.View {

        className() {
            return "zpp-extra zpp-columns"
        }

        events() {
            return {
                "click .zpp-columns__btn" : "onClick"
            }
        }

        initialize() {
            this.render();
        }

        setUpEventListeners(){
            
        }

        render() {
            this.$el.append($("<div class='zpp-columns__inner'>"));
            this.$(".zpp-columns__inner").append($("<div class='zpp-btn zpp-columns__btn'>AUTO</div>"));
        }

        /**
         * 
         */
        checkCorrectTask(){
            return $(_customViewHolder).text().trim().toLowerCase() == "dd view";
        }

        /* Clear any previous instances of this View's activation (if any). */
        /*clearRender() {
            $(".zpp-projectgroup__group-header").remove();
        }*/

        _awaitSubtaskFormMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_form)[0], resolve, 500);
            });
        }

        _awaitSubtaskListMutation() {
            return new Promise((resolve) => {
                ZPP.onLastMutation($(_subtaskList)[0], resolve, 0);
            });
        }

        async onClick() {

            // Toggle each setting.
            $(_column).each((i, e) => {

                // Get the name of the column.
                let name = $(e).text().toLowerCase().trim();

                // Determine if the input has been toggled.
                let $input = $(e).find(_columnSwitchInput);
                let checked = $input[0].hasAttribute("checked");

                // Toggle the switch input.
                let isTargetColumn = _.includes(COLUMNS, name);

                if (isTargetColumn != checked) {
                    if (checked) {
                        $input.removeAttr("checked");
                    }
                    else {
                        $input.attr("checked", true);
                    }
                }

                // Record the index that this column should be in (it will be used for sorting).
                $(e).attr("data-zpp-index", isTargetColumn ? _.indexOf(COLUMNS, name) : -1);
            });

            // Sort the columns that were toggled on.
            $(_column).sort(function(a,b) {
                return parseInt($(a).attr("data-zpp-index"),10) - parseInt($(b).attr("data-zpp-index"),10);
            }).appendTo(_columnContainer);

            // Show the save element and then click the save button.
            $(_saveContainer).removeClass("hide").show();
            
            await ZPP.sleep(250); // Just to be safe.
                
            $(_saveBtn)[0].click(); // And save the layout.
        }
    }

    class ColumnsSingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.
            
            // Wait for the popup element to be toggled to appear.
            ZPP.onMutation($(_popup)[0], () => {
                
                $(_container).find(".zpp-columns").remove();
                $(_container).append(new ColumnsView({model: this}).el);

                // The "Subtask" tab was just activated; create the view.
                /*if ($(_tabContainerSubtask).hasClass("active")){

                    // Fallback: clear any existing zpp-extras.
                    $(_container).find(".zpp-extra").remove();
                    $(_container).append(new ScreensSubtasksView({model: this}).el);
                }*/

            });
        }
    }

    let columns = new ColumnsSingleton();
    return columns;
});