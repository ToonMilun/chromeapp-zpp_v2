define([
    "zpp"
  ], function(ZPP) {

    /**
     * Screens Subtasks Generator
     * --------------------------
     * 
     * Creates a button in the "Subtasks" tab of the "Screens" task of a Subject project.
     * When clicked, will add a number of XXXX-X-X screen subtasks. 
     */

    const LOCATION = "buginfo";      // Only activate if in this router location.

    // Selectors
    // ---------------
    const _bugDesc = "#iframeDescrp";             // Selector for the bug description.

    class BugsPlusSingleton extends Backbone.Model {

        initialize() {
            this.setUpEventListeners();
        }

        setUpEventListeners(){
            this.listenTo(ZPP, "project:postRender", this.onProjectPostRender);
        }

        onProjectPostRender($project) {

            if (!ZPP.matchRouterLocation(LOCATION)) return; // Only activate if in this router location.

            let $desc = $(_bugDesc);
            $desc.contents().find("#zpp-styles").remove();
            $desc.contents().find("body").append("<style id='zpp-styles'>strike,s{opacity:0.35;}</style>");

            console.log("Bugs-plus: added styles to bug description.");
        }
    }

    let bugsPlusSingleton = new BugsPlusSingleton();
    return bugsPlusSingleton;
});