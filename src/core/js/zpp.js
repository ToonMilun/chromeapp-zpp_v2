define([
	"core/js/logging"
], function(Logging){
	
	/**
	 * This object can be globally accessed to listen for common custom events and/or access global objects. 
	 */

	/**
	 * List of events:
	 * 
	 * zoho:ready					- Triggered only once (when the Zoho page has fully loaded).
	 * router:location				- Triggered whenever the location changes.
	 * project:postRender			- Triggered whenever the #projectcontent element mutates after a router:location event.
	 * 								  This element contains the content for each tab (Dashboard, Tasks, Bugs, etc.) in Zoho.
	 */

	const DEBOUNCE_DURATION = 500; // In ms.
	const MUTATION_DEBOUNCE_DURATION = DEBOUNCE_DURATION;

	class ZPPSingleton extends Backbone.Model {

		defaults(){
			return {
				_isReady: false // Set to true once zoho:ready has triggered.
			}
		}

		log = Logging; // Make the Logging object accessible to anything which has access to the ZPP object.

		initialize() {
			
			this.setUpDebounce();
			this.setUpEventListeners();
		}

		/**
		 * Creates a Mutation Observer which will disconnect itself after firing.
		 * 
		 * This observer is designed to detect the "last" mutation on the el within a set period of time.
		 * It does this by adding a slight delay before executing the callback.
		 * If another mutation is observed in that time, then it will reset the delay.
		 * 
		 * If no mutation is detected after a set timeout, then trigger the event anyway.
		 * TODO: Allow customization of the fallbackTimeout duration.
		 */
		onLastMutation(el, callback, fallbackTimeoutDuration) {
			this.onMutation(el, callback, fallbackTimeoutDuration === undefined ? MUTATION_DEBOUNCE_DURATION * 4 : fallbackTimeoutDuration, true);
		}

		sleep(duration) {
			return new Promise(resolve => {
				_.debounce(resolve, duration)();
			});
		}

		onMutation(el, callback, fallbackTimeoutDuration = 0, once = false) {

			let m;
			let _this = this;
			let d = _.debounce(() => {
				callback.bind(_this)();
				if (once) m.disconnect();
			}, MUTATION_DEBOUNCE_DURATION);

			let fallbackTimeout;

			// Create the mutation observer
			// ----------------------------
			m = new MutationObserver(() => {
				// As soon as any mutation is observed, cancel the fallback timeout.
				if (fallbackTimeout) {
					clearTimeout(fallbackTimeout);
					fallbackTimeout = null;
				}
				d();
			});
			m.observe(el, {
				attributes: true,
				childList: true,
				characterData: true
			});

			// Instant disconnect conditions
			// -----------------------------
			// Changing the router location should always disconnect MOs.
			this.listenToOnce(this, "router:location", () => {m.disconnect();});
			// Fallback to call the callback after a set duration.
			if (fallbackTimeoutDuration) {
				fallbackTimeout = setTimeout(() => {
					callback.bind(_this)();
					if (once) m.disconnect();
				}, fallbackTimeoutDuration);
			}
		}

		$getProjectContent(){
			return $("#projectcontent"); // Update this selector if core Zoho gets updated.
		}

		/**
		 * Certain functions need to be debounced to prevent repeated triggering.
		 */
		setUpDebounce() {
			// Give a slight delay (just in case) before declaring the #projectcontent element ready for inspection.
			this.listenForProjectPostRender = _.debounce(this.listenForProjectPostRender.bind(this), DEBOUNCE_DURATION);
		}

		setUpEventListeners() {

			if (document.readyState === "complete") {
				Logging.event("zoho:ready");
				this.trigger("zoho:ready");
				this.set("_isReady", true);
			}
			else {
				window.onload = () => {
					Logging.event("zoho:ready");
					this.trigger("zoho:ready");
					this.set("_isReady", true);
				}
			}

			// Fallback: the MutationObserver for #projectcontent won't trigger on initial pageload;
			// This ensures that the initial instance of the event still triggers.
			this.listenTo(this, "zoho:ready", () => {
				this.trigger("project:postRender", this.$getProjectContent());
			});
			this.listenTo(this, "router:location", this.onRouterLocationChange);
			this.listenTo(this, "project:postRender", this.onProjectPostRender);
		}

		// Use with:
		// await ZPP.isReadyPromise();
		// To wait for the event to have triggered before continuing.
		isReadyPromise() {
			return new Promise((resolve) => {
				if (this.get("_isReady")) resolve();
				else {
					this.listenToOnce(this, "zoho:ready", resolve);
				}
			});
		}

		onProjectPostRender($el) {
			Logging.event("project:postRender");
			/*$(".tasklist-expandcollapse-container").each(function(){
				_.debounce(() => {console.log("GO!"); $(this)[0].click();}, 1000)();
			});*/
		}

		// Returns true if matchStr is equal to the current location.
		matchRouterLocation(matchStr) {
			return this.get("_routerLocation").toLowerCase() == matchStr.toLowerCase();
		}

		async onRouterLocationChange(location) {

			this.set("_routerLocation", location);
			Logging.event("router:location", location);

			await this.isReadyPromise(); // Wait for the page to be fully loaded before starting mutation observers.

			this.listenForProjectPostRender();
		}

		listenForProjectPostRender(){
			let $projectcontent = this.$getProjectContent();
			this.onLastMutation($projectcontent[0], () => {
				this.trigger("project:postRender", $projectcontent);
			});
		}

		// Lock screen (prevent the user from clicking on it).
		lockZoho(){

			$(".zpp-lockscreen").remove();

			let $lock = $("<div class='zpp-lockscreen'>");
			$lock.html("\
				<div class='zpp-lockscreen__content'>\
					<div class='zpp-lockscreen__content-inner'>\
						<h1>Please wait...</h1>\
						<h3>Zoho++ is running a macro and should not be interrupted. If you suspect something has gone wrong, then (and only then) should you click the 'X' in the top-right.</h3>\
						<div class='zpp-lockscreen__close'>X</div>\
					</div>\
				</div>");

			$("body").append($lock);
			$lock.find(".zpp-lockscreen__close").one("click", () => {
				this.unlockZoho();
			});

			Logging.debug("Zoho locked");
		}

		unlockZoho() {
			$(".zpp-lockscreen").remove();
			Logging.debug("Zoho unlocked");
		}

		/**
		 * Get data stored to the extension (in background.js).
		 * @param {*} dataName 
		 * @param {*} callback 
		 */
		getChromeData(dataName, callback) {
			return new Promise(resolve => {
				chrome.storage.local.get(dataName, function(data){
					if (callback) callback(data[dataName]);
					resolve();
				});
			});
		}

		/**
		 * Set global Chrome app data.
		 * @param {*} dataObj 
		 * @param {*} callback 
		 */
		setChromeData(dataObj, callback){
			chrome.storage.local.set(dataObj, callback);
		}
	}

	let zpp = new ZPPSingleton();
	return zpp;
});







