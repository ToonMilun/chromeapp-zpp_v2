define([
    "zpp"
], function(ZPP) {

    /**
     * router.js
     * 
     * Monitors for changes to the URL.
     */
    
    class MainRouterSingleton extends Backbone.Router {

        initialize() {
            Backbone.history.start(); // Tells Backbone to start watching for hashchange events
        }

        /**
         * All Backbone Routes
         */
        routes() {
            return {
                "*other": "pageRoute", // Specific routes don't seem to work. Need to listen for any URL changes and then regex match instead.
            }
        }

        pageRoute(pageName) {
            ZPP.trigger("router:location", pageName.split("/")[0]);
        }
    }

    let router = new MainRouterSingleton();
    return router;
});