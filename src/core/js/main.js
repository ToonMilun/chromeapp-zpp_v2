define([
	"libraries/handlebars"
], function(Handlebars){
	
	// Global variables
    // ----------------
    // These are referred to constantly.
    window.Handlebars = Handlebars;
	
	// Start the global object and start.
	// "Injector!" must be used whenever the require() function is called like this.  
	require(["Injector!zpp"], (ZPP) => {

		// Start everything.
		require(["Injector!core/js/start"], (Start) => {
		});
	});
});







