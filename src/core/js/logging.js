define([
    "core/js/enums/logLevelEnum"
], function(LOG_LEVEL) {

    /**
     * Script is a modification of Adapt Framework 3.0's logging.js.
     */

    var Logging = Backbone.Model.extend({

        _config: {
            _isEnabled: true,
            _console: true // Log to console
        },  
        
        success: function() {
            this._log("SUCCESS", Array.prototype.slice.call(arguments));
        },

        scorm: function() {
            this._log("SCORM", Array.prototype.slice.call(arguments));
        },
                
        debug: function() {            
            this._log("DEBUG", Array.prototype.slice.call(arguments));
        },

        event: function() {            
            this._log("EVENT", Array.prototype.slice.call(arguments));
        },

        warn: function() {
            this._log("WARN", Array.prototype.slice.call(arguments));
        },
        
        error: function() {
            this._log("ERROR", Array.prototype.slice.call(arguments));
        },

        message: function() {
            this._log("MESSAGE", Array.prototype.slice.call(arguments));
        },
        
        _log: function(level, data) {

            var isEnabled = (this._config._isEnabled);
            if (!isEnabled) return;

            this._logToConsole(level, data);

            // Allow error reporting plugins to hook and report to logging systems
            this.trigger('log', level, data);
            this.trigger('log:' + level.asLowerCase, level, data);
        },

        _logToConsole: function(level, data) {

            var shouldLogToConsole = (this._config._console);
            if (!shouldLogToConsole) return;

            var log = ["%c" + level.toUpperCase() + ''];
            if (data) log.push.apply(log, data);

            // Apply special colouration for each log type.
            var c = "font-weight: bold; text-decoration: underline overline; " + LOG_LEVEL[level];
            log.splice(1, 0, c);

            // is there a matching console method we can use e.g. console.error()?
            if(console[level.toLowerCase()]) {
                console[level.toLowerCase()].apply(console, log);
            } else {
                console.log.apply(console, log);
            }
        }

    });

    var logging = new Logging();

    // See: appData.js for where AppData.log is set.
    return logging;
});
