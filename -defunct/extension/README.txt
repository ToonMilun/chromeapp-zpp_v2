Scripts that handle the interaction of the Chrome Extension with the Interface window. Mainly used to:
- Create the interface window when the Extension "play" button is clicked.
- Re-link with the interface window whenever a page is changed, and pass information about the current window to the Interface window to allow it to continue running its scripts.