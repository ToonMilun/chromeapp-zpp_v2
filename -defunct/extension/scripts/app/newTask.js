/**
 * Set up a completely new task. Clear previous values if necessary.
 */

define(["utils", "interface", "main"], function(u, Interface, Main){

	return function(){
		
		function init(){

			// Clear the value so that any future page changes will pick up the continuity.
			u.setChromeData({newTask: false});

			// Create a brand new popup window, then initialize Main (it will get a reference to the window upon loading).
			new Interface(true, function(){new Main();});
		}

		init();
	};
});







